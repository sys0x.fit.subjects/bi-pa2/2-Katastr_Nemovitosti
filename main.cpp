#include <utility>

#include <utility>

#include <utility>

#include <utility>

#ifndef __PROGTEST__

#include <cstring>
#include <cstdlib>
#include <cstdio>
#include <cctype>
#include <cmath>
#include <cassert>
#include <iostream>
#include <iomanip>
#include <string>
#include <vector>
#include <list>
#include <algorithm>
#include <functional>
#include <memory>

using namespace std;
#endif /* __PROGTEST__ */

class Land;

class Key {
    string city;
    string addr;
    string reg;
    unsigned int id;
public:
    Key(const string &_city, const string &_addr, const string &_reg, unsigned int _id) {
        city = _city;
        addr = _addr;
        reg = _reg;
        id = _id;
    }

    const string &getCity() const {
        return city;
    }

    const string &getAddr() const {
        return addr;
    }

    const string &getReg() const {
        return reg;
    }

    int getId() const {
        return id;
    }

    void setCity(const string &city) {
        Key::city = city;
    }

    void setAddr(const string &addr) {
        Key::addr = addr;
    }

    void setReg(const string &reg) {
        Key::reg = reg;
    }

    void setId(unsigned int id) {
        Key::id = id;
    }
};

enum type {
    OWNER, REGID, CITYADDR
};

class Land {
private:


    string _city;
    string _addr;
    string _region;
    unsigned int _id;

    string _ownerRealName;
    string _ownerSmallName;


public:
    Land(string _city, string _addr, string _region, unsigned int _id,
         const string &_ownerRealName)
            : _city(std::move(_city)), _addr(std::move(_addr)), _region(std::move(_region)), _id(_id),
              _ownerRealName(_ownerRealName) {

        _ownerSmallName = _ownerRealName;
        transform(_ownerSmallName.begin(), _ownerSmallName.end(), _ownerSmallName.begin(), ::towlower);
    }

    const string &get_ownerRealName() const {
        return _ownerRealName;
    }

    void set_ownerName(const string &_ownerRealName) {
        Land::_ownerRealName = _ownerRealName;
        _ownerSmallName = _ownerRealName;
        transform(_ownerSmallName.begin(), _ownerSmallName.end(), _ownerSmallName.begin(), ::towlower);

    }

    const string &get_ownerSmallName() const {
        return _ownerSmallName;
    }

    const string &get_city() const {
        return _city;
    }

    const string &get_addr() const {
        return _addr;
    }

    const string &get_region() const {
        return _region;
    }

    unsigned int get_id() const {
        return _id;
    }


};

bool compareCityAddr(const Land *left, const Key &right) {

    int comp = right.getCity().compare(left->get_city());

    if (comp == 0) {
        comp = right.getAddr().compare(left->get_addr());
        return comp > 0;
    } else return comp > 0;

}

bool compareRegId(const Land *left, const Key &right) {
    bool equals = static_cast<unsigned int>(right.getId()) == left->get_id();

    int comp = static_cast<unsigned int>(right.getId()) > static_cast<unsigned int>(left->get_id());

    if (equals) {
        comp = right.getReg().compare(left->get_region());
        return comp > 0;
    } else return static_cast<bool>(comp);

}

bool compareOwner(const vector<Land *> &left, const string &right) { //...

    return right > left[0]->get_ownerSmallName();
}


class CIterator {
public:
    explicit CIterator(vector<Land *> newIterLand) {
        _iterLand = std::move(newIterLand);
    }

    bool AtEnd() const {
        return _iterLand.size() <= i;

    }

    void Next() {

        i++;


    }

    string City() const {
        if (!AtEnd()) {
            return _iterLand[i]->get_city();
        }
        return "";
    }

    string Addr() const {
        if (!AtEnd()) {
            return _iterLand[i]->get_addr();
        }
        return "";
    }

    string Region() const {
        if (!AtEnd()) {
            return _iterLand[i]->get_region();
        }
        return "";
    }

    unsigned ID() const {
        if (!AtEnd()) {
            return _iterLand[i]->get_id();
        }
        return 0;
    }

    string Owner() const {
        if (!AtEnd()) {
            return _iterLand[i]->get_ownerRealName();
        }
        return "";
    }

private:
    vector<Land *> _iterLand;
    unsigned int i = 0;

};

class CLandRegister {

private:
    vector<Land *> _vecCityAddr;
    vector<Land *> _vecRegionId;
    vector<vector<Land *>> _vecOwners;

    long long int getOwner(const string &ownerName) const {
        auto ownersSize = static_cast<long long int>(_vecOwners.size());

        long long int ownerNumber;

        ownerNumber = lower_bound(_vecOwners.begin(), _vecOwners.end(), ownerName, compareOwner) - _vecOwners.begin();

        if (ownerNumber < 0 || ownerNumber >= ownersSize) {
            return -1;
        }

        if (_vecOwners[ownerNumber][0]->get_ownerSmallName() != ownerName) {
            return -1;
        }
        return ownerNumber;

    }

    Land *getLand(vector<Land *> vec, const Key &key, type enumType) const {


        auto sizeOfLands = static_cast<long long int>(vec.size());
        long long int landNumber = -1;

        if (enumType == CITYADDR) {
            landNumber = lower_bound(vec.begin(), vec.end(), key, compareCityAddr) - vec.begin();
        } else if (enumType == REGID) {
            landNumber = lower_bound(vec.begin(), vec.end(), key, compareRegId) - vec.begin();
        }

        if (landNumber < 0 || landNumber >= sizeOfLands) {
            return nullptr;
        }

        if ((vec[landNumber]->get_city() != key.getCity() || vec[landNumber]->get_addr() != key.getAddr()) &&
            enumType == CITYADDR) {
            return nullptr;
        } else if ((vec[landNumber]->get_region() != key.getReg() || vec[landNumber]->get_id() != static_cast<unsigned int>(key.getId())) &&
                   enumType == REGID) {
            return nullptr;
        }
        return vec[landNumber];
    }

    void addOwner(Land *landToAdd, const string &_newOwnerSmallName, const string &_oldOwnerSmallName) {

        long long int oldOwnerPosition = getOwner(_oldOwnerSmallName);
        long long int newOwnerPosition = getOwner(_newOwnerSmallName);
        Key temp(landToAdd->get_city(), landToAdd->get_addr(), landToAdd->get_region(), landToAdd->get_id());
        if (oldOwnerPosition != -1) {
            eraseLand(_vecOwners[oldOwnerPosition], landToAdd, temp, OWNER);
        }

        if (newOwnerPosition == -1) {
            vector<Land *> newOwner;
            newOwner.push_back(landToAdd);
            _vecOwners.insert(lower_bound(_vecOwners.begin(), _vecOwners.end(), _newOwnerSmallName, compareOwner),
                              newOwner);

        } else {
            _vecOwners[newOwnerPosition].push_back(landToAdd);
        }

        if (oldOwnerPosition != -1) {
            if (_vecOwners[oldOwnerPosition].empty()) {
                _vecOwners.erase(_vecOwners.begin() + oldOwnerPosition);
            }
        }


    }

    bool eraseLand(vector<Land *> &vecDel, Land *landToDelete, const Key &key, type typeOfDel) {
        auto sizeOfVector = static_cast<long long int>(vecDel.size());
        long long int check = -1;

        if (typeOfDel == CITYADDR) {
            check = lower_bound(vecDel.begin(), vecDel.end(), key, compareCityAddr) - vecDel.begin();
        } else if (typeOfDel == REGID) {
            check = lower_bound(vecDel.begin(), vecDel.end(), key, compareRegId) - vecDel.begin();
        } else if (typeOfDel == OWNER) {
            for (unsigned int i = 0; i < vecDel.size(); i++) {
                if (vecDel[i]->get_city() == key.getCity() && vecDel[i]->get_addr() == key.getAddr()) {
                    check = static_cast<long long int>(i);
                }
            }
        }

        if (check < 0 || check >= sizeOfVector) {
            return true;
        }
        vecDel.erase(vecDel.begin() + check);
        return vecDel.empty();


    }

public:
    bool Add(const string &city,
             const string &addr,
             const string &region,
             unsigned int id) {

        Land *newLand = new Land(city, addr, region, id, "");

        Key temp(city, addr, region, id);

        Land *foundLandRegId = getLand(_vecRegionId, temp, REGID);
        Land *foundLandCityAddr = getLand(_vecCityAddr, temp, CITYADDR);

        if (foundLandCityAddr != nullptr || foundLandRegId != nullptr) {
            delete newLand;
            return false;
        }
//        int CA=lower_bound(_vecCityAddr.begin(), _vecCityAddr.end(),temp, compareCityAddr)-_vecCityAddr.begin();
//        int RI=lower_bound(_vecRegionId.begin(), _vecRegionId.end(), temp, compareRegId)-_vecRegionId.begin();

//        if(_vecCityAddr.size()==1){
//            _vecCityAddr.insert(_vecCityAddr.begin(),newLand);
//        }else{
        _vecCityAddr.insert(lower_bound(_vecCityAddr.begin(), _vecCityAddr.end(), temp, compareCityAddr), newLand);
        _vecRegionId.insert(lower_bound(_vecRegionId.begin(), _vecRegionId.end(), temp, compareRegId), newLand);

        addOwner(newLand, "", "");


        return true;
    }


    bool Del(const string &city,
             const string &addr) {

        Key temp(city, addr, "noreg", 0);
        Land *aboutToDeleteLand = getLand(_vecCityAddr, temp, CITYADDR);
        if (aboutToDeleteLand == nullptr) {
            return false;
        }

        temp.setReg(aboutToDeleteLand->get_region());
        temp.setId(aboutToDeleteLand->get_id());

        long long int ovnerPosition = getOwner(aboutToDeleteLand->get_ownerSmallName());
        if (ovnerPosition < 0) {
            return false;
        }

        if (eraseLand(_vecOwners[ovnerPosition], aboutToDeleteLand, temp, OWNER)) {
            _vecOwners.erase(_vecOwners.begin() + ovnerPosition);
        }
        eraseLand(_vecCityAddr, aboutToDeleteLand, temp, CITYADDR);
        eraseLand(_vecRegionId, aboutToDeleteLand, temp, REGID);


        delete aboutToDeleteLand;


        return true;
    }

    bool Del(const string &region,
             unsigned int id) {
        Key temp("", "", region, id);

        Land *aboutToDeleteLand = getLand(_vecRegionId, temp, REGID);
        if (aboutToDeleteLand == nullptr) {
            return false;
        }


        temp.setAddr(aboutToDeleteLand->get_addr());
        temp.setCity(aboutToDeleteLand->get_city());

        long long int ovnerPosition = getOwner(aboutToDeleteLand->get_ownerSmallName());
        if (ovnerPosition < 0) {
            return false;
        }

        if (eraseLand(_vecOwners[ovnerPosition], aboutToDeleteLand, temp, OWNER)) {
            _vecOwners.erase(_vecOwners.begin() + ovnerPosition);
        }
        eraseLand(_vecCityAddr, aboutToDeleteLand, temp, CITYADDR);
        eraseLand(_vecRegionId, aboutToDeleteLand, temp, REGID);


        delete aboutToDeleteLand;

        return true;
    }

    bool GetOwner(const string &city,
                  const string &addr,
                  string &owner) const {
        Key temp(city, addr, "noreg", 0);

        Land *foundLand = getLand(_vecCityAddr, temp, CITYADDR);
        if (foundLand == nullptr) {
            return false;
        }

        owner = foundLand->get_ownerRealName();
        return true;
    }

    bool GetOwner(const string &region,
                  unsigned int id,
                  string &owner) const {

        Key temp("", "", region, id);

        Land *foundLand = getLand(_vecRegionId, temp, REGID);
        if (foundLand == nullptr) {
            return false;
        }
        owner = foundLand->get_ownerRealName();

        return true;
    }


    bool NewOwner(const string &city,
                  const string &addr,
                  const string &owner) {

        string _ownerSmallName = owner;
        transform(_ownerSmallName.begin(), _ownerSmallName.end(), _ownerSmallName.begin(), ::towlower);

        Key temp(city, addr, "noreg", 0);
        Land *foundLand = getLand(_vecCityAddr, temp, CITYADDR);
        if (foundLand == nullptr) {
            return false;
        }

        if (foundLand->get_ownerSmallName() == _ownerSmallName) {
            return false;
        }

        string _oldOwnerSmallName = foundLand->get_ownerSmallName();
        addOwner(foundLand, _ownerSmallName, _oldOwnerSmallName);

        foundLand->set_ownerName(owner);
        return true;
    }

    bool NewOwner(const string &region,
                  unsigned int id,
                  const string &owner) {

        string _ownerSmallName = owner;
        transform(_ownerSmallName.begin(), _ownerSmallName.end(), _ownerSmallName.begin(), ::towlower);
        Key temp("", "", region, id);

        Land *foundLand = getLand(_vecRegionId, temp, REGID);

        if (foundLand == nullptr) {
            return false;
        }
        if (foundLand->get_ownerSmallName() == _ownerSmallName) {
            return false;
        }

        string oldOwner = foundLand->get_ownerSmallName();
        addOwner(foundLand, _ownerSmallName, oldOwner);
        foundLand->set_ownerName(owner);


        return true;
    }

    unsigned Count(const string &owner) const {
        string _ownerSmallName = owner;
        transform(_ownerSmallName.begin(), _ownerSmallName.end(), _ownerSmallName.begin(), ::towlower);
        long long int ownerPosition = getOwner(_ownerSmallName);
        if (ownerPosition != -1) {
            return _vecOwners[ownerPosition].size();
        }

        return 0;
    }

    CIterator ListByAddr() const {
        return CIterator(_vecCityAddr);
    }

    CIterator ListByOwner(const string &owner) const {
        string _ownerSmallName = owner;
        transform(_ownerSmallName.begin(), _ownerSmallName.end(), _ownerSmallName.begin(), ::towlower);

        long long int ownerPosition = getOwner(_ownerSmallName);
        if (ownerPosition != -1) {
            return CIterator(_vecOwners[ownerPosition]);
        }
        return CIterator(vector<Land *>());
    }

};

#ifndef __PROGTEST__

static void test0() {
    CLandRegister x;
    string owner;
    assert (!x.NewOwner("", 4552, "Cvut"));
    assert (!x.GetOwner("Prague", "Thakurova", owner));
    assert (!x.GetOwner("Dejvice", 12345, owner));
    assert (!x.GetOwner("Prague", "Evropska", owner));
    assert (!x.GetOwner("Vokovice", 12345, owner));
    assert (!x.GetOwner("Prague", "Technicka", owner));
    assert (!x.Del("Prague", "Thakurova"));
    assert (!x.Del("Prague", "Thakurova"));
    assert (!x.Del("Prague", "Thakurova"));
    assert (!x.Del("Prague", "Thakurova"));
    assert (!x.Del("Prague", "Thakurova"));
    assert (!x.GetOwner("Neexijici mesto", "Netuj ulice", owner));

    assert (!x.Del("Prague", 54352));


    assert (x.Add("", "", "", 0));
    assert (x.Del("", ""));
    assert (x.Add("", "", "", 0));
    assert (x.Del("", 0));
    assert (x.Add("", "", "", 0));
    assert (x.Del("", 0));
    CIterator i54 = x.ListByOwner("");
    assert (x.Add("", "", "", 0));
    i54.Next();

    i54.Next();
//    assert ( x . Add ( "Prague", "Thakurova", "Dejvice", 12345 ) );
//    assert ( x . Add ( "Prague", "Evropska", "Vokovice", 12345 ) );
//    assert ( x . Add ( "Prague", "Technicka", "Dejvice", 9873 ) );
//    assert ( x . Add ( "Plzen", "Evropska", "Plzen mesto", 78901 ) );
//    assert ( x . Add ( "Liberec", "Evropska", "Librec", 4552 ) );
//
//    assert (x.NewOwner("Prague", "Thakurova", "CVUT1"));
//    assert (x.NewOwner("Dejvice", 9873, "CVUT2"));
//    assert (x.NewOwner("Plzen", "Evropska", "Anton Hrabis3"));
//    assert (x.NewOwner("Librec", 4552, "Cvut4"));
//
//    assert ( x . Add ( "Prague1", "Thakurova", "Dejvice1", 12345 ) );
//    assert ( x . Add ( "Prague1", "Evropska", "Vokovice1", 12345 ) );
//    assert ( x . Add ( "Prague1", "Technicka", "Dejvice1", 9873 ) );
//    assert ( x . Add ( "Plzen1", "Evropska", "Plzen mesto1", 78901 ) );
//    assert ( x . Add ( "Liberec1", "Evropska", "Librec1", 4552 ) );
//
//    assert (x.NewOwner("Prague1", "Thakurova", "CVUT5"));
//    assert (x.NewOwner("Dejvice1", 9873, "CVUT6"));
//    assert (x.NewOwner("Plzen1", "Evropska", "Anton Hrabis7"));
//    assert (x.NewOwner("Librec1", 4552, "Cvut8"));
//
//    assert ( x . Add ( "Prague2", "Thakurova", "Dejvice2", 12345 ) );
//    assert ( x . Add ( "Prague2", "Evropska", "Vokovice2", 12345 ) );
//    assert ( x . Add ( "Prague2", "Technicka", "Dejvice2", 9873 ) );
//    assert ( x . Add ( "Plzen2", "Evropska", "Plzen mesto2", 78901 ) );
//    assert ( x . Add ( "Liberec2", "Evropska", "Librec2", 4552 ) );
//
//    assert (x.NewOwner("Prague2", "Thakurova", "CVUT9"));
//    assert (x.NewOwner("Dejvice2", 9873, "CVUT10"));
//    assert (x.NewOwner("Plzen2", "Evropska", "Anton Hrabis11"));
//    assert (x.NewOwner("Librec2", 4552, "Cvut12"));
//
//
    i54.Owner();
    assert (x.Del("", 0));
//    assert ( x . Add ( "Prague3", "Thakurova", "Dejvice3", 12345 ) );
//    assert ( x . Add ( "Prague3", "Evropska", "Vokovice3", 12345 ) );
//    assert ( x . Add ( "Prague3", "Technicka", "Dejvice3", 9873 ) );
//    assert ( x . Add ( "Plzen3", "Evropska", "Plzen mesto3", 78901 ) );
//    assert ( x . Add ( "Liberec3", "Evropska", "Librec3", 4552 ) );
//    assert (x.NewOwner("Prague3", "Thakurova", "aaaa"));
//    assert (x.NewOwner("Dejvice3", 9873, "%$^10"));
//    assert (x.NewOwner("Plzen3", "Evropska", "@#$!"));
//    assert (x.NewOwner("Librec3", 4552, "&&&&&"));
    assert (x.Add("Prague", "Thakurova", "Dejvice", 12345));
    assert (x.Add("Prague", "Evropska", "Vokovice", 12345));
    assert (x.Add("Prague", "Technicka", "Dejvice", 9873));
    assert (x.Add("Plzen", "Evropska", "Plzen mesto", 78901));
    assert (x.Add("Liberec", "Evropska", "Librec", 4552));
//    assert ( x . Add ( "", "", "", 0 ) );
//    assert ( x . Add ( "", " ", "", 1 ) );
//    assert ( x . Add ( " ", "", "", 2 ) );
    CIterator i0 = x.ListByAddr();
    assert (!i0.AtEnd()
            && i0.City() == "Liberec"
            && i0.Addr() == "Evropska"
            && i0.Region() == "Librec"
            && i0.ID() == 4552
            && i0.Owner() == "");
    i0.Next();
    assert (!i0.AtEnd()
            && i0.City() == "Plzen"
            && i0.Addr() == "Evropska"
            && i0.Region() == "Plzen mesto"
            && i0.ID() == 78901
            && i0.Owner() == "");
    i0.Next();
    assert (!i0.AtEnd()
            && i0.City() == "Prague"
            && i0.Addr() == "Evropska"
            && i0.Region() == "Vokovice"
            && i0.ID() == 12345
            && i0.Owner() == "");
    i0.Next();
    assert (!i0.AtEnd()
            && i0.City() == "Prague"
            && i0.Addr() == "Technicka"
            && i0.Region() == "Dejvice"
            && i0.ID() == 9873
            && i0.Owner() == "");
    i0.Next();
    assert (!i0.AtEnd()
            && i0.City() == "Prague"
            && i0.Addr() == "Thakurova"
            && i0.Region() == "Dejvice"
            && i0.ID() == 12345
            && i0.Owner() == "");
    i0.Next();
    assert (i0.AtEnd());

    assert (x.Count("") == 5);
    CIterator i1 = x.ListByOwner("");
    assert (!i1.AtEnd()
            && i1.City() == "Prague"
            && i1.Addr() == "Thakurova"
            && i1.Region() == "Dejvice"
            && i1.ID() == 12345
            && i1.Owner() == "");
    i1.Next();
    assert (!i1.AtEnd()
            && i1.City() == "Prague"
            && i1.Addr() == "Evropska"
            && i1.Region() == "Vokovice"
            && i1.ID() == 12345
            && i1.Owner() == "");
    i1.Next();
    assert (!i1.AtEnd()
            && i1.City() == "Prague"
            && i1.Addr() == "Technicka"
            && i1.Region() == "Dejvice"
            && i1.ID() == 9873
            && i1.Owner() == "");
    i1.Next();
    assert (!i1.AtEnd()
            && i1.City() == "Plzen"
            && i1.Addr() == "Evropska"
            && i1.Region() == "Plzen mesto"
            && i1.ID() == 78901
            && i1.Owner() == "");
    i1.Next();
    assert (!i1.AtEnd()
            && i1.City() == "Liberec"
            && i1.Addr() == "Evropska"
            && i1.Region() == "Librec"
            && i1.ID() == 4552
            && i1.Owner() == "");
    i1.Next();
    assert (i1.AtEnd());

    assert (x.Count("CVUT") == 0);
    CIterator i2 = x.ListByOwner("CVUT");
    assert (i2.AtEnd());

    assert (x.NewOwner("Prague", "Thakurova", "CVUT"));
    assert (x.NewOwner("Dejvice", 9873, "CVUT"));
    assert (x.NewOwner("Plzen", "Evropska", "Anton Hrabis"));
    assert (x.NewOwner("Librec", 4552, "Cvut"));
    assert (x.GetOwner("Prague", "Thakurova", owner) && owner == "CVUT");
    assert (x.GetOwner("Dejvice", 12345, owner) && owner == "CVUT");
    assert (x.GetOwner("Prague", "Evropska", owner) && owner == "");
    assert (x.GetOwner("Vokovice", 12345, owner) && owner == "");
    assert (x.GetOwner("Prague", "Technicka", owner) && owner == "CVUT");
    assert (x.GetOwner("Dejvice", 9873, owner) && owner == "CVUT");
    assert (x.GetOwner("Plzen", "Evropska", owner) && owner == "Anton Hrabis");
    assert (x.GetOwner("Plzen mesto", 78901, owner) && owner == "Anton Hrabis");
    assert (x.GetOwner("Liberec", "Evropska", owner) && owner == "Cvut");
    assert (x.GetOwner("Librec", 4552, owner) && owner == "Cvut");
    CIterator i3 = x.ListByAddr();
    assert (!i3.AtEnd()
            && i3.City() == "Liberec"
            && i3.Addr() == "Evropska"
            && i3.Region() == "Librec"
            && i3.ID() == 4552
            && i3.Owner() == "Cvut");
    i3.Next();
    assert (!i3.AtEnd()
            && i3.City() == "Plzen"
            && i3.Addr() == "Evropska"
            && i3.Region() == "Plzen mesto"
            && i3.ID() == 78901
            && i3.Owner() == "Anton Hrabis");
    i3.Next();
    assert (!i3.AtEnd()
            && i3.City() == "Prague"
            && i3.Addr() == "Evropska"
            && i3.Region() == "Vokovice"
            && i3.ID() == 12345
            && i3.Owner() == "");
    i3.Next();
    assert (!i3.AtEnd()
            && i3.City() == "Prague"
            && i3.Addr() == "Technicka"
            && i3.Region() == "Dejvice"
            && i3.ID() == 9873
            && i3.Owner() == "CVUT");
    i3.Next();
    assert (!i3.AtEnd()
            && i3.City() == "Prague"
            && i3.Addr() == "Thakurova"
            && i3.Region() == "Dejvice"
            && i3.ID() == 12345
            && i3.Owner() == "CVUT");
    i3.Next();
    assert (i3.AtEnd());

    assert (x.Count("cvut") == 3);
    CIterator i4 = x.ListByOwner("cVuT");
    assert (!i4.AtEnd()
            && i4.City() == "Prague"
            && i4.Addr() == "Thakurova"
            && i4.Region() == "Dejvice"
            && i4.ID() == 12345
            && i4.Owner() == "CVUT");
    i4.Next();
    assert (!i4.AtEnd()
            && i4.City() == "Prague"
            && i4.Addr() == "Technicka"
            && i4.Region() == "Dejvice"
            && i4.ID() == 9873
            && i4.Owner() == "CVUT");
    i4.Next();
    assert (!i4.AtEnd()
            && i4.City() == "Liberec"
            && i4.Addr() == "Evropska"
            && i4.Region() == "Librec"
            && i4.ID() == 4552
            && i4.Owner() == "Cvut");
    i4.Next();
    assert (i4.AtEnd());

    assert (x.NewOwner("Plzen mesto", 78901, "CVut"));
    assert (x.Count("CVUT") == 4);
    CIterator i5 = x.ListByOwner("CVUT");
    assert (!i5.AtEnd()
            && i5.City() == "Prague"
            && i5.Addr() == "Thakurova"
            && i5.Region() == "Dejvice"
            && i5.ID() == 12345
            && i5.Owner() == "CVUT");
    i5.Next();
    assert (!i5.AtEnd()
            && i5.City() == "Prague"
            && i5.Addr() == "Technicka"
            && i5.Region() == "Dejvice"
            && i5.ID() == 9873
            && i5.Owner() == "CVUT");
    i5.Next();
    assert (!i5.AtEnd()
            && i5.City() == "Liberec"
            && i5.Addr() == "Evropska"
            && i5.Region() == "Librec"
            && i5.ID() == 4552
            && i5.Owner() == "Cvut");
    i5.Next();
    assert (!i5.AtEnd()
            && i5.City() == "Plzen"
            && i5.Addr() == "Evropska"
            && i5.Region() == "Plzen mesto"
            && i5.ID() == 78901
            && i5.Owner() == "CVut");
    i5.Next();
    assert (i5.AtEnd());

    assert (x.Del("Liberec", "Evropska"));
    assert (x.Del("Plzen mesto", 78901));
    assert (x.Count("cvut") == 2);
    CIterator i6 = x.ListByOwner("cVuT");
    assert (!i6.AtEnd()
            && i6.City() == "Prague"
            && i6.Addr() == "Thakurova"
            && i6.Region() == "Dejvice"
            && i6.ID() == 12345
            && i6.Owner() == "CVUT");
    i6.Next();
    assert (!i6.AtEnd()
            && i6.City() == "Prague"
            && i6.Addr() == "Technicka"
            && i6.Region() == "Dejvice"
            && i6.ID() == 9873
            && i6.Owner() == "CVUT");
    i6.Next();
    assert (i6.AtEnd());

    assert (x.Add("Liberec", "Evropska", "Librec", 4552));
    CIterator i7 = x.ListByOwner("");
    i7.Next();
    assert (x.Del("Librec", 4552));
    assert (x.Del("Prague", "Thakurova"));
    assert (x.Del("Vokovice", 12345));
    assert (x.Del("Dejvice", 9873));
    assert (!x.Del("Plzen", "Evropska"));
    assert (!x.Del("Liberec", "Evropska"));
}

static void test1() {
    CLandRegister x;
    string owner;

    assert (x.Add("Prague", "Thakurova", "Dejvice", 12345));
    assert (x.Add("Prague", "Evropska", "Vokovice", 12345));
    assert (x.Add("Prague", "Technicka", "Dejvice", 9873));
    assert (!x.Add("Prague", "Technicka", "Hradcany", 7344));
    assert (!x.Add("Brno", "Bozetechova", "Dejvice", 9873));
    assert (!x.GetOwner("Prague", "THAKUROVA", owner));
    assert (!x.GetOwner("Hradcany", 7343, owner));
    CIterator i0 = x.ListByAddr();
    assert (!i0.AtEnd()
            && i0.City() == "Prague"
            && i0.Addr() == "Evropska"
            && i0.Region() == "Vokovice"
            && i0.ID() == 12345
            && i0.Owner() == "");
    i0.Next();
    assert (!i0.AtEnd()
            && i0.City() == "Prague"
            && i0.Addr() == "Technicka"
            && i0.Region() == "Dejvice"
            && i0.ID() == 9873
            && i0.Owner() == "");
    i0.Next();
    assert (!i0.AtEnd()
            && i0.City() == "Prague"
            && i0.Addr() == "Thakurova"
            && i0.Region() == "Dejvice"
            && i0.ID() == 12345
            && i0.Owner() == "");
    i0.Next();
    assert (i0.AtEnd());

    assert (x.NewOwner("Prague", "Thakurova", "CVUT"));
    assert (!x.NewOwner("Prague", "technicka", "CVUT"));
    assert (!x.NewOwner("prague", "Technicka", "CVUT"));
    assert (!x.NewOwner("dejvice", 9873, "CVUT"));
    assert (!x.NewOwner("Dejvice", 9973, "CVUT"));
    assert (!x.NewOwner("Dejvice", 12345, "CVUT"));
    assert (x.Count("CVUT") == 1);
    CIterator i1 = x.ListByOwner("CVUT");
    assert (!i1.AtEnd()
            && i1.City() == "Prague"
            && i1.Addr() == "Thakurova"
            && i1.Region() == "Dejvice"
            && i1.ID() == 12345
            && i1.Owner() == "CVUT");
    i1.Next();
    assert (i1.AtEnd());

    assert (!x.Del("Brno", "Technicka"));
    assert (!x.Del("Karlin", 9873));
    assert (x.Del("Prague", "Technicka"));
    assert (!x.Del("Prague", "Technicka"));
    assert (!x.Del("Dejvice", 9873));
    assert (x.Del("Prague", "Evropska"));
    assert (x.Del("Prague", "Thakurova"));
}

int main() {
    test0();
    test1();
    return 0;
}

#endif /* __PROGTEST__ */
